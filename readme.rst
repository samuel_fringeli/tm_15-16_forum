#####
Application web - forum
#####

Ce dépôt contient le code-source d'un forum codé essentiellement en ``javascript``.

Il est possible de lire un tutoriel qui explique comment le créer en cliquant `ici`_ ou de télécharger une version PDF de ce tutoriel à l'aide de `ce lien`_

Si vous souhaitez installer cette application sur votre ordinateur, vous devez installer les logiciels suivants :

- Un interpréteur ``nodeJS``
- Le système de gestion de base de données ``MySQL``

Ensuite, après avoir téléchargé cette application, exécutez la commande suivante :

.. code-block:: sh

	npm install

Puis, créez une base de données à l'aide des instructions qui se trouvent dans le fichier ``database.sql``. Enfin, renseignez les informations sur votre base de données dans la fonction ``exports.pool()`` du fichier ``./serveur/sql.js``.

Vous pouvez alors mettre en marche l'application :

.. code-block:: sh

	sudo nodejs app.js

.. liens
.. _ici: http://j.mp/tm_forum_vf
.. _ce lien: http://j.mp/tm_forum_pdf