CREATE TABLE `discussions` (
  `id_discussion` int(11) AUTO_INCREMENT,
  PRIMARY KEY (`id_discussion`),
  `sujet` varchar(255)
) DEFAULT CHARSET=utf8;

CREATE TABLE `likes` (
  `id_message` int(11),
  `nom_utilisateur` varchar(255) 
) DEFAULT CHARSET=utf8;

CREATE TABLE `messages` (
  `id_message` int(11) AUTO_INCREMENT,
  PRIMARY KEY (`id_message`),
  `nom_utilisateur` varchar(255) ,
  `contenu` text ,
  `date_ecriture` datetime,
  `date_modification` datetime,
  `id_discussion` int(11),
  `likes` int(11) DEFAULT '0'
) DEFAULT CHARSET=utf8;

CREATE TABLE `session` (
  `nom_utilisateur` varchar(255) ,
  `token` varchar(255) 
) DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id_utilisateur` int(11) AUTO_INCREMENT,
  PRIMARY KEY (`id_utilisateur`),
  `nom_utilisateur` varchar(255) ,
  `mot_de_passe` varchar(255) ,
  `email` varchar(255) ,
  `avatar` int(11) DEFAULT '1',
  `nom` varchar(255),
  `prenom` varchar(255)
) DEFAULT CHARSET=utf8;

INSERT INTO `discussions` (`id_discussion`, `sujet`) 
  VALUES(1, 'Première discussion'),(2, 'Deuxième discussion');

INSERT INTO `messages` (`id_message`, `nom_utilisateur`, `contenu`, `date_ecriture`, `date_modification`, `id_discussion`, `likes`) 
  VALUES
    (1, 'samf', 'Bonjour ! Ceci est le premier message de ce forum :)', '2016-01-13 15:57:56', '0000-00-00 00:00:00', 1, 0),
    (2, 'samf', 'Bonjour ! Est-ce possible d'envoyer des images dans ce forum ?'', '2016-01-13 15:59:40', '0000-00-00 00:00:00', 2, 0);

INSERT INTO `users` (`id_utilisateur`, `nom_utilisateur`, `mot_de_passe`, `email`, `avatar`, `nom`,`prenom`) 
  VALUES
    (1, 'samf', 'hello', 'first@me.com', 1, 'First', 'User');