var http = require('http');
var path = require('path');
var crypto = require('crypto');
var base64url = require('base64url');
var mysql = require('mysql');
var express = require('express');
var app = express();
var server = http.Server(app);
var io = require('socket.io')(server);
var express_session = require('express-session');

var routes = require('./serveur/routes.js');
var session = require('./serveur/session.js');
var socket = require('./serveur/socket.js');
var sql = require('./serveur/sql.js');

routes.f(app, session, express, path, __dirname, mysql, sql, express_session, crypto, base64url);
socket.f(io, mysql, sql, session, crypto, base64url);

server.listen(8888);