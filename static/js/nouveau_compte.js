// crée un nouveau compte
function nouveau_compte() {
	if (verifier('utilisateur') && 
		verifier('prenom') && 
		verifier('nom') && 
		verifier('mail') && 
		verifier('password')) 
	{
	
		socket.emit('nouveau_compte', { 
			nom_utilisateur : byId('nc_utilisateur').value,
			password : byId('nc_password').value,
			prenom : byId('nc_prenom').value,
			nom : byId('nc_nom').value,
			mail : byId('nc_mail').value
		});
	}
}

// affiche un message d'erreur si l'utilisateur existe
socket.on('utilisateur_existant', function() {
	byId('utilisateur_existant').style.display = 'block';
});