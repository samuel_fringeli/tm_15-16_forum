var socket = io.connect('http://' + window.location.host);

function byId(id) {
	return document.getElementById(id);
}

function byClass(classe) {
	return document.getElementsByClassName(classe);
}

function init_editeur(id_editeur) {
	var tinymce_toolbar = "undo redo | \
	n eqneditor | \
	n underline bold italic | \
	n alignleft aligncenter alignright alignjustify | \
	n bullist numlist outdent indent | \
	n image link code | \
	n forecolor backcolor emoticons";
	var tinymce_plugins = "eqneditor textcolor image code emoticons";
	tinyMCE.init({
			selector: '#'+id_editeur,
			plugins: tinymce_plugins,
			toolbar: tinymce_toolbar,
			language: "fr_FR"
	});
}

function navbar_mobile() {
	if (byId('contenu_nav').className === 'navbar-collapse collapse') {
		byId('contenu_nav').className = 'navbar-collapse collapse in';
		byId('navbar').style.height = '100%';
	} else {
		byId('contenu_nav').className = 'navbar-collapse collapse';
		byId('navbar').style.height = '50px';
	}
}