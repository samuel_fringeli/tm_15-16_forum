// publie un message depuis une nouvelle discussion
function publier_message(nom_utilisateur) {    
  if (byId('input_sujet').value !== '' &&
    tinyMCE.get('emplacement_nouveau_message').getContent() !== '') {
      socket.emit('nouvelle_discussion', { 
      	sujet : byId('input_sujet').value, 
      	message : tinyMCE.get('emplacement_nouveau_message').getContent(), 
      	nom_utilisateur : nom_utilisateur 
    });
 	}
}

// redirige l'utilisateur vers la nouvelle discussion
socket.on('redirection', function(id_discussion) {
	window.location.assign(id_discussion);
});