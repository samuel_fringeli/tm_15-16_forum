var id_discussion = window.location.href.replace(/.+\/([0-9]+)/, '$1');

/* fonction permettant de formatter les guillemets simples et doubles
ainsi que les backslash pour éviter un bug lors de la requête SQL */
function echap(chaine) {
  return chaine
    .replace(/"/g, "#g_doubles")
    .replace(/'/g, "#g_simples")
    .replace(/\\/g, "#backslash")
}

// fonction qui réalise l'inverse de la précédente
function unechap(chaine) {
  return chaine
    .replace(/#g_doubles/g, '"')
    .replace(/#g_simples/g, "'")
    .replace(/#backslash/g, "\\");
}

function publier_message(nom_utilisateur) {
  if (tinyMCE.get('emplacement_nouveau_message').getContent() !== '') {
    socket.emit('nouveau_message', {
     nom_utilisateur : nom_utilisateur,
     contenu : echap(tinyMCE.get('emplacement_nouveau_message').getContent()),
     id_discussion : id_discussion
    });
    tinyMCE.get('emplacement_nouveau_message').setContent('');
  }
}

function supprimer_message(id_message) {
  socket.emit('suppression_message_serveur', {
    id_discussion : id_discussion,
    id_message : id_message
  });
}

/* "like" d'un message
à noter que cette fonction envoie également le nom d'utilisateur
pour l'insérer dans la table "likes" de la base de données
et ainsi éviter qu'un utilisateur puisse aimer 2X un message */
function aimer(id_message, nom_utilisateur) {
  socket.emit('like_message', { 
    id_message : id_message,
    nom_utilisateur : nom_utilisateur,
    id_discussion : id_discussion
  });
}

/* retourne simplement l'objet javascript de la classe "zone_message"
à partir d'un objet contenant une propriété "id_message" */
function zone_message(classe, d) {
  return byId(d.id_message)
    .getElementsByClassName('zone_message')[0]
    .getElementsByClassName(classe)[0];
}

/* affiche l'éditeur TinyMCE à la place du contenu du message
masque le bouton "modifier" et affiche le bouton "enregistrer" */
function modifier_message(id_message) {
  var d = { id_message : id_message };
  byId(id_message+'_bouton_modifier').style.display = 'none';
  byId(id_message+'_bouton_modifier_mobile').style.display = 'none';
  byId(id_message+'_bouton_enregistrer').style.display = 'block';
  byId(id_message+'_bouton_enregistrer_mobile').style.display = 'inline';
  zone_message('affichage', d).style.display = 'none';
  zone_message('editeur', d).style.display = 'block';
  // récupère l'ID aléatoire de TinyMCE
  var id_tinymce = zone_message('editeur', d).getElementsByTagName('textarea')[0].id;
  init_editeur(id_tinymce);
}

/* enregistre le message, envoie un événement socket, masque le bouton "enregistrer", 
affiche le bouton "modifier". A noter que le message est modifié sur la page une
fois que socket.io retourne l'événement "update_message" */
function enregistrer_message(id_message) {
  var d = { id_message : id_message };
  byId(id_message+'_bouton_modifier').style.display = 'block';
  byId(id_message+'_bouton_modifier_mobile').style.display = 'inline';
  byId(id_message+'_bouton_enregistrer').style.display = 'none';
  byId(id_message+'_bouton_enregistrer_mobile').style.display = 'none';
  zone_message('affichage', d).style.display = 'block';
  zone_message('editeur', d).style.display = 'none';
  var id_tinymce = zone_message('editeur', d).getElementsByTagName('textarea')[0].id;
  socket.emit('modification_message', {
    contenu: echap(tinyMCE.get(id_tinymce).getContent()),
    id_message: id_message,
    id_discussion : id_discussion
  });
}

/* télécharge le dernier message à l'aide d'une requête AJAX
s'exécute lorsqu'un utilisateur a publié un message sur la discussion */
socket.on('charger_dernier_message'+id_discussion, function() {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'dernier_'+id_discussion);
  xhr.addEventListener('readystatechange', function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      byId('messages').innerHTML += xhr.responseText;
      var dates_ecriture = byId('messages').getElementsByClassName('date_ecriture');
      var id = dates_ecriture[dates_ecriture.length-1]
        .getElementsByTagName('span')[0].id;
      convertir_date(Date.now(), id);
    }
  });
  xhr.send(null);
});

// supprime un message du côté client 
socket.on('suppression_message_client_'+id_discussion, function(id_message) {
  byId(id_message).style.display = 'none';
});

// met à jour les likes d'un message
socket.on('update_likes'+id_discussion, function(m) {
  byId(m.id_message).getElementsByClassName('likes')[0].innerHTML = m.nombre_likes;
  byId(m.id_message).getElementsByClassName('likes')[1].innerHTML = m.nombre_likes;
})

/* met à jour le contenu d'un message, change l'ID de la textarea pour que le 
message puisse toujours être modifié */
socket.on('update_message'+id_discussion, function(d) {
  zone_message('date_modification', d).innerHTML = 'modifié le ' + 
    convertir_date(Date.now());
  zone_message('editeur', d).innerHTML = '<textarea id="'+Date.now()+'">' + 
    unechap(d.contenu)+'</textarea>';
  zone_message('affichage', d).innerHTML = unechap(d.contenu);
});