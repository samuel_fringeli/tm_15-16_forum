/* change de classe bootstrap la balise <div> contenant
les informations sur l'utilisateur pour que l'avatar puisse
pendre toute la largeur de l'écran */
function responsive() {
	if (window.matchMedia("(max-width: 400px)").matches) {
	  byId('infos_profil').className = 'col-xs-12';
	}
	else {
	  byId('infos_profil').className = 'col-xs-8';
	}

	if (!window.matchMedia("(min-width: 399px)").matches) {
	  byId('infos_profil').className = 'col-xs-12';
	}
	else {
	  byId('infos_profil').className = 'col-xs-8';
	}
}

// vérifie le formulaire de modification de données
// entoure les champs correspondants en vert
function verifier_form() {
	verifier('prenom');
	verifier('nom');
	verifier('mail');
}

// affiche le formulaire de modification des coordonnées
function afficher_mod() {
	byId('mod_password').style.display = 'none';
	byId('mod_avatar').style.display = 'none';
	if (byId('mod_coordonnees').style.display === 'none') {
	  byId('mod_coordonnees').style.display = 'block';
	}
	else {
	  byId('mod_coordonnees').style.display = 'none';
	}
}

// affiche le formulaire de modification du mot de passe
function afficher_mod_password() {
	byId('mod_coordonnees').style.display = 'none';
	byId('mod_avatar').style.display = 'none';
	if (byId('mod_password').style.display === 'none') {
	  byId('mod_password').style.display = 'block';
	}
	else {
	  byId('mod_password').style.display = 'none';
	}
}

// affiche le choix d'avatars
function afficher_mod_avatar() {
	byId('mod_password').style.display = 'none';
	byId('mod_coordonnees').style.display = 'none';
	if (byId('mod_avatar').style.display === 'none') {
		byId('mod_avatar').style.display = 'block';
	}
	else {
		byId('mod_avatar').style.display = 'none';
	}
}

// envoie un événement socket pour modifier les coordonnées
function modifier_compte(utilisateur) {
	if (verifier('prenom') && 
		verifier('nom') && 
		verifier('mail'))
	{
		socket.emit('modifier_compte', {
			utilisateur : utilisateur, 
			prenom : byId('nc_prenom').value,
			nom : byId('nc_nom').value,
			mail : byId('nc_mail').value,
		});
	}
}

// envoie un événement socket pour modifier le mot de passe
function modifier_password(utilisateur) {
	if (byId('nc_ancien_password').value !== '' && verifier('nouveau_password')) {
		socket.emit('modifier_password', {
			nom_utilisateur : utilisateur,
			ancien_password : byId('nc_ancien_password').value,
			nouveau_password : byId('nc_nouveau_password').value
		});
	}
}

// télécharge les petits avatars et les affiche sur la page
function avatars_small(nom_utilisateur) {
	for (var i = 1; i < 101; i++) {
		byId('mod_avatar').innerHTML += 
			'<image src="static/avatars_petits/'+i+
			'.jpg" onclick="changer_avatar('+i+', \''+nom_utilisateur+'\')">';
	}
}

// change l'avatar d'un utilisateur sur la page et envoie un événement socket
function changer_avatar(n, nom_utilisateur) {
	byClass('avatar_profil_desktop')[0].innerHTML = 
		'<image src="static/avatars_grands/'+n+'.jpg" class="avatar_profil"></image>';
	byClass('avatar_profil_mobile')[0].innerHTML = 
		'<image src="static/avatars_grands/'+n+'.jpg" class="avatar_profil"></image>';
	socket.emit('changement_avatar', {
		avatar : n,
		nom_utilisateur : nom_utilisateur
	});
}

// affiche un message d'erreur - mot de passe incorrect
socket.on('ancien_password_incorrect', function() {
	byId('form_nc_ancien_password').className = 'form-group has-error';
	byId('ancien_password_incorrect').style.display = 'inline';
});

/* réinitialise les champs des mots de passe et affiche un message confirmant
la modification du mot de passe */
socket.on('mod_password_ok', function() {
	byId('form_nc_ancien_password').className = 'form-group';
	byId('nc_ancien_password').value = '';
	byId('nc_nouveau_password').value = '';
	byId('ancien_password_incorrect').style.display = 'none';
	byId('mod_password').style.display = 'none';
	verifier('nouveau_password');
	byId('password_success').style.display = 'block';
	byId('coordonnees_success').style.display = 'none';
});

/* actualise les nouvelles coordonnées et affiche un message confirmant
la modification des coordonnées chez l'utilisateur qui les a modifiées */
socket.on('mod_compte_ok', function(nouveau) {
    byId('info_prenom').innerHTML = 'Prénom : ' + nouveau.prenom;
    byId('info_nom').innerHTML = 'Nom : ' + nouveau.nom;
    byId('info_email').innerHTML = 'E-mail : ' + nouveau.mail;
    if (!nouveau.broadcast) {
	    byId('nc_prenom').value = nouveau.prenom;
	    byId('nc_nom').value = nouveau.nom;
	    byId('nc_prenom_mobile').value = nouveau.prenom;
	    byId('nc_nom_mobile').value = nouveau.nom;
	    byId('nc_mail').value = nouveau.mail;
	    byId('mod_coordonnees').style.display = 'none';
	    byId('password_success').style.display = 'none';
		byId('coordonnees_success').style.display = 'block';
	}
});