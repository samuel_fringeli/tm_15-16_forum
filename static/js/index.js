socket.on('charger_derniere_discussion', function() {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'derniere_discussion');
	xhr.addEventListener('readystatechange', function() {
		if (xhr.readyState === 4 && xhr.status === 200) {
			byId('retour_ajax').innerHTML += xhr.responseText;
		}
	});
	xhr.send(null);
});

function rechercher() {
	byId('chargement_discussions').innerHTML = '';
	byId('retour_ajax').innerHTML = '';
	var recherche = byId('recherche_discussion').value;
  	var discussions = byClass('discussion');
  	for (var i = 0; i < discussions.length; i++) {
  		discussions[i].style.display = 'none';
  	}
  	for (var i = 0; i < discussions.length; i++) {
  		var sujet = discussions[i].getElementsByClassName('sujet_texte')[0].innerHTML;
  		var regex = new RegExp(recherche, 'i');
  		if (regex.test(sujet)) {
  			discussions[i].style.display = 'block';
  		}
  	}
}