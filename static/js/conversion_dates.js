convertir_date = function(date, id_html) {
	date = new Date(date);
	var jour_mois = date.getDate();
	var jour = date.getDay();
	var mois = date.getMonth();
	var annee = date.getFullYear();
	var heure = date.getHours();
	var minutes = date.getMinutes();
	
	switch(jour) {
		case 1: jour = 'lundi'; break;
		case 2: jour = 'mardi'; break;
		case 3: jour = 'mercredi'; break;
		case 4: jour = 'jeudi'; break;
		case 5: jour = 'vendredi'; break;
		case 6: jour = 'samedi'; break;
		case 0: jour = 'dimanche'; break;
	}
	
	switch(mois) {
		case 0:  mois = 'janvier'; break;
		case 1:  mois = 'février'; break;
		case 2:  mois = 'mars'; break;
		case 3:  mois = 'avril'; break;
		case 4:  mois = 'mai'; break;
		case 5:  mois = 'juin'; break;
		case 6:  mois = 'juillet'; break;
		case 7:  mois = 'août'; break;
		case 8:  mois = 'septembre'; break;
		case 9:  mois = 'octobre'; break;
		case 10: mois = 'novembre'; break;
		case 11: mois = 'décembre'; break;
	}

	if(/^[0-9]$/.test(minutes)) {
		date_francais = jour+' '+jour_mois+' '+mois+' '+annee+' à '+heure+'h0'+minutes;
	}
	else {
		date_francais = jour+' '+jour_mois+' '+mois+' '+annee+' à '+heure+'h'+minutes;
	}
	
	if(typeof(id_html) === 'undefined') {
		return date_francais;
	}
	else {
		byId(id_html).innerHTML = date_francais;
	}
}