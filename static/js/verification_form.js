function get_regex(data) {
	switch(data) {
		case 'utilisateur': 
			return /^[a-z0-9]{4,10}$/i;
		case 'password': case 'nouveau_password':
			return /^.{4,20}$/i;
		case 'prenom': case 'prenom_mobile': case 'nom': case 'nom_mobile':
			return /^[a-zâäàéèùêëîïôöçñ\ ]{2,50}$/i;
		case 'mail':
			return /^[a-z0-9\.-_]+\@[a-z0-9\.-_]+\.[a-z]{2,10}$/i;
	}
}

function get_html_class(data, type) {
	if (data === 'prenom' || data === 'nom') {
		switch(type) {
			case 'success':
				return 'has-success';
			case 'nothing':
				return '';
			case 'error':
				return 'has-error';
		}
	} else {
		switch(type) {
			case 'success':
				return 'form-group has-success';
			case 'nothing':
				return 'form-group';
			case 'error':
				return 'form-group has-error';
		}
	}
}

function verifier(data, stop) {
	var return_value;
	if (get_regex(data).test(byId('nc_'+data).value)) {
		byId('form_nc_'+data).className = get_html_class(data, 'success');
		byId(data+'_incorrect').style.display = 'none';
		return_value = true;
	} else if (byId('nc_'+data).value === '') {
		byId('form_nc_'+data).className = get_html_class(data, 'nothing');
		byId(data+'_incorrect').style.display = 'none';
		return_value = false;
	} else {
		byId('form_nc_'+data).className = get_html_class(data, 'error');
		byId(data+'_incorrect').style.display = 'inline';
		return_value = false;
	}
	if ((data === 'prenom' || data == 'nom') && !stop) {
		byId('nc_'+data+'_mobile').value = byId('nc_'+data).value;
		verifier(data+'_mobile', true);
	}
	if ((data === 'prenom_mobile' || data == 'nom_mobile') && !stop) {
		byId('nc_'+data.replace(/_mobile/, '')).value = byId('nc_'+data).value;
		verifier(data.replace(/_mobile/, ''), true);
	}
	return return_value;
}

